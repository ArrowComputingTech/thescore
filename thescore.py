#This script copies, renames, archives, and then deletes a user's newest songs from their Dropbox folder.

import os
import platform
import time
import glob
import shutil
import stat
from subprocess import call

#Set user/userPath here, make this the same as the dropbox folder name:
user='RainbowDRGN'
showName='DragonShow'
userPath = os.path.join('/Users/DJ/Dropbox',user)
userPathFiles = os.path.join('/Users/DJ/Dropbox',user,'*')
externalPath = os.path.join('/Volumes/Music/DJ Audio Files',showName)

#Delete Icon? file (will automagically regenerate itself later via Dropbox.)
#Runs every file check, in case it reappears during file operation:
def deleteIcon(file):
 try:
  fileD = 'Icon?'
  call(["rm",fileD])
  return 0
 except FileNotFoundError:
  return 1

#4.
#Copy newestFile to external drive, rename, delete from Dropbox:
def manipulateFiles(newestFile,currentListOfFiles,i):
  #Copy newestFile to external drive:
  shutil.copy2(newestFile, '/Volumes/Music/DJ Audio Files/DragonShow')
  #Remove newesFile from currentListOfFiles:
  currentListOfFiles.remove(newestFile)
  #Remove newestFile from Dropbox:
  os.remove(newestFile)
  return (currentListOfFiles)

#3.
#Checks newest file to see if size remains constant over time.sleep(x) seconds.
#This can be used to delay manipulating files eg. if they are in the middle of being downloaded:
def checkForUploading(checkFile):
 fileStat = os.stat(checkFile)
 time.sleep(1)
 #Compares initial file size with file size after time.sleep delay.
 #Returns file if good, runs process again until file sizes are the same if not:
 if (fileStat.st_size == os.stat(checkFile).st_size):
  return (checkFile)
 else:
  checkForUploading(checkFile)

#2.
#Set newestFile to newest file via mod time:
def getNewest(currentListOfFiles):
 newestFile = max(currentListOfFiles, key=os.path.getctime)
 #Terminate if the Icon? file or hidden file (begins with a .) is newest file:
 if newestFile.startswith('.'):
  print ('Finished.')
  exit()
 else:
  return (checkForUploading(newestFile))

#1.
#Get list of files in userPath, delete 'Icon?' file, get newest file, copy to archive:
def moveFiles():
 print ('Getting list of files...')
 listOfFiles = glob.glob(userPathFiles)
 i=2
 for file in listOfFiles:
  currentListOfFiles = glob.glob(userPathFiles)
  #Determine newest file in currentListOfFiles:
  newestFile = getNewest(currentListOfFiles)
  #Delete file if it is Icon? file:
  deleteIcon(newestFile)
  #Copy newestFile to external drive and delete it from Dropbox:
  currentListOfFiles = manipulateFiles(newestFile,currentListOfFiles,i)
  #Decrement counter, used to number files.
  if i != 0:
   i-=1

moveFiles()
print('Moved files. Hopefully.')
exit()
